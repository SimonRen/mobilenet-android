import numpy as np
import matplotlib.pyplot as plt
import keras
from keras.models import load_model
from keras.utils.generic_utils import CustomObjectScope

ModelName='MobileNet'
TestImg='images/test1.png'

# Define relu6 for the keras model (The model cannot be loaded otherwise)
def relu6(x):
  return K.relu(x, max_value=6)
  
# Load image
im = plt.imread(TestImg)
# Load labels
Text_file = open('ImageNetClass.txt', "r")
Labels=Text_file.readlines()


with CustomObjectScope({'relu6': relu6}): # definition of the relu6 element
	model = load_model('MobileNet')
Result=model.predict([[im]])
Max=np.amax(Result,axis=1)
MaxIndex=Result.argmax(axis=1)
MaxIndex=MaxIndex[0]
print(Max*100)
Label=Labels[MaxIndex]
print(Label)