import tensorflow as tf
import numpy as np
import argparse

def get_args():
	parser = argparse.ArgumentParser(description='set input arguments')
	parser.add_argument('--input', action="store", type=str, required=True,
	help="name (or path if not in the same folder) of the input pb model, ex: WideResNet112.pb")
	args=parser.parse_args()
	return args
	
def main():
	args=get_args()
	path_to_model=args.input
	graph_def = tf.GraphDef()
	with tf.Session() as sess:
	# Import the TF graph
		with tf.gfile.FastGFile(path_to_model, 'rb') as f:
			graph_def.ParseFromString(f.read())
			sess.graph.as_default()
			tf.import_graph_def(graph_def, name='')
			Layers= [n.name for n in tf.get_default_graph().as_graph_def().node]
			print(Layers)
			
if __name__=='__main__':
	main()