# Python 3.6.0
# tensorflow 1.11.0
# Keras 2.2.4
from keras.applications.mobilenet import MobileNet

# Load model and weights
model=MobileNet(include_top=True, weights='imagenet',classes=1000)
model.save('MobileNet')
# Get layer input and output name with:
# print(model.summary())

